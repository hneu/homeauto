#!/usr/bin/python
# -*- coding: utf-8 -*-
# Version 2.0.0

import os, math, time, threading
from datetime import datetime
from datetime import datetime
from unitContainer import CValuesUnit
from unitContainer import CUnitContainer

#///////////////////////////////////////////////////////////////////////////////////////

def getTimeStamp():
	now = datetime.now()
	t = now.strftime( "%H:%M:%S" )
	return t

#///////////////////////////////////////////////////////////////////////////////////////

class CDataHandler( object ):
	def __init__( self, aThreadingTime ):
		self.mUpdateTime = aThreadingTime
		self.messageContainer = []

		self.mA1C24 = CUnitContainer( 1440 ) 	# Container ability 1, bitmask 00000001, 24 hours
		self.mA2C24 = CUnitContainer( 1440 ) 	# Container ability 2, bitmask 00000010, 24 hours
		self.mA3C24 = CUnitContainer( 1440 ) 	# Container ability 3, bitmask 00000100, 24 hours
		self.mA1C72 = CUnitContainer( 4320 ) 	# Container ability 1, bitmask 00000001, 72 hours
		self.mA2C72 = CUnitContainer( 4320 )	# Container ability 2, bitmask 00000010, 72 hours
		self.mA3C72 = CUnitContainer( 4320 )	# Container ability 3, bitmask 00000100, 72 hours
		threading.Timer( self.mUpdateTime, self.onUpdate ).start()
		self.ips = [ "ip"] * 20
		self.names = ["name"] * 20
		self.lastUpdates = [0] * 20
		self.updates = [0] * 20
		self.startTime = [0.0] * 20

#-------------------------------------------------------------------------------------

	def onUpdate( self ):
		print( "onUpdate at", getTimeStamp() )
		self.checkUpdates()
		self.dataTransmit( 0b00000001,  7, 12, self.mA1C24 ) #reading temperature in range [7:12] when t ability is set
		self.dataTransmit( 0b00000010, 12, 17, self.mA2C24 ) #reading huminitiy in range [12:17] when h ability is set
		self.dataTransmit( 0b00000100, 17, 22, self.mA3C24 ) #reading module data in range [17:22] when module ability ist set
		
		self.dataTransmit( 0b00000001,  7, 12, self.mA1C72 ) #reading temperature in range [7:12] when t ability is set
		self.dataTransmit( 0b00000010, 12, 17, self.mA2C72 ) #reading huminitiy in range [12:17] when h ability is set
		self.dataTransmit( 0b00000100, 17, 22, self.mA3C72 ) #reading module data in range [17:22] when module ability ist set

		self.messageContainer = []
		threading.Timer( self.mUpdateTime, self.onUpdate ).start()

#-------------------------------------------------------------------------------------

	def checkUpdates( self ):
		index = -1
		valuesUnit = CValuesUnit()
		for item in self.messageContainer:
			# check if huminity/temperature module, module index 10
			if item[0:3] == "@10":
				index = int( item[3:5] ) - 10
				# check if index is valid
				if index in valuesUnit.getValidIndexes():
					self.updates[index] += 1

#-------------------------------------------------------------------------------------
	
	def dataTransmit( self, aCapacity, aLeftPosition, aRightPosition, aContainer ):
		print( "dataTransmit at", getTimeStamp() )
		valuesUnit = CValuesUnit()
		values = [ 0.0 ] * 20
		valid = [ 0 ] * 20
		index = -1
		for item in self.messageContainer:
			# check if huminity/temperature module, module index 10
			if item[0:3] == "@10":
				# isolate the capacity of the telegram and check if the desired value is available, this is bitwise encoded
				capacity = int( item[5:7], 16 )
				if capacity & aCapacity:
					# get value from the telegram position
					value = float( item[aLeftPosition:aRightPosition] )
					# find module address and store value and valid flag depending. The module address starts with 10
					index = int( item[3:5] ) - 10
					# check if index is valid
					if index in valuesUnit.getValidIndexes():
						values[index] = value
						valid[index] = 1
						self.ips[index] = item[22:34]
						self.names[index] = item[1:5]
						self.lastUpdates[index] = time.time()
						if self.startTime[index] == 0.0:
							self.startTime[index] = time.time()

		if 1 in valid:
			valuesUnit.setValues( values )
			valuesUnit.setValidMask( valid )
			#aContainer.appendUnit( valuesUnit )
			aContainer.mergeLastUnit( valuesUnit )

#-------------------------------------------------------------------------------------

	def appendData( self, aData ):
		if aData[0:1] == "@":
			self.messageContainer.append( aData )
			print( "message: " , aData, " at ", getTimeStamp() )

#-------------------------------------------------------------------------------------

	def getModuleNames( self, aIndexes ):
		self.container = []
		for index in aIndexes:
			self.container.append ( self.names[index] )
		return self.container
		
#-------------------------------------------------------------------------------------

	def getModuleIpAddress( self, aIndexes ):
		self.container = []
		for index in aIndexes:
			self.container.append ( self.ips[index] )
		return self.container
		
#-------------------------------------------------------------------------------------

	def getModuleLastUpdates( self, aIndexes ):
		self.container = []
		for index in aIndexes:
			self.container.append ( self.lastUpdates[index] )
		return self.container
		
#-------------------------------------------------------------------------------------

	def getModuleUpdates( self, aIndexes ):
		self.container = []
		for index in aIndexes:
			self.container.append ( self.updates[index] )
		return self.container
		
#-------------------------------------------------------------------------------------

	def getModuleStartTime( self, aIndexes ):
		self.container = []
		startTime = ""
		for index in aIndexes:
			if self.startTime[index] != 0.0:
				startTime = datetime.fromtimestamp( self.startTime[index] ).strftime( "%A %H:%M" )
			else:
				startTime = "-"
			self.container.append ( startTime )
		return self.container
		
#-----------------------------------------------------------------

	def dataAnalysis( self ):
		print( "---------------------------------------------------------------------------------------------------------------------------------------------------------------" )
		print( "Data Analysis" )
		print( "---------------------------------------------------------------------------------------------------------------------------------------------------------------" )		
		print( "Global minimum temperature:", self.mA1C24.getMinimum() )
		print( "Global maximum temperature:", self.mA1C24.getMaximum() )
		print( "\n")
		for item in self.mA1C24.getUnits():
			print( "--Temperatures: ", item.getValues(), "at", datetime.fromtimestamp( item.getTimeStamp()  ).strftime( "%A %H:%M:%S" ) )

		print( "\n")
		print( "Global minimum huminty:", self.mA2C24.getMinimum() )
		print( "Global maximum huminty:", self.mA2C24.getMaximum() )
		for item in self.mA2C24.getUnits():
			print( "--Huminities: ", item.getValues(), "at", datetime.fromtimestamp( item.getTimeStamp()  ).strftime( "%A %H:%M:%S" ) )

		print( "\n")
		print( "Global minimum Vcc:", self.mA3C24.getMinimum() )
		print( "Global maximum Vcc:", self.mA3C24.getMaximum() )
		for item in self.mA3C24.getUnits():
			print( "--Vcc: ", item.getValues(), "at", datetime.fromtimestamp( item.getTimeStamp()  ).strftime( "%A %H:%M:%S" )  )

		print( "\n")
		print( "---------------------------------------------------------------------------------------------------------------------------------------------------------------" )
