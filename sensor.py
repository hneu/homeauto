import os, math
from datetime import datetime
from espdata import CEspData
from module import CModule

class Csvg( object ):

	def __init__( self, aName, aDim, aWidth, aHeight, aBufferSize ):
		self.mName = aName
		self.mBufferSize = aBufferSize
		self.mWidth = aWidth
		self.mHeight = aHeight
		self.mDim = aDim
		self.mData = []
		self.mDate = []
		self.mDay = []
		self.timeMin = ""
		self.timeMax = ""
		self.mDayMax = ""
		self.mDayMin = ""
		self.mTrace = False
		self.mEpsilon = 0.001
		self.mLastY = 0;
		self.mLastData = 0

		self.mActiveWidth = aWidth - 50
		if self.mActiveWidth < 0:
			self.mActiveWidth = aWidth

		self.mActiveHeight = aHeight - 50

		self.mYmargin = 50
		self.mXmargin = 10

#-------------------------------------------------------
	def getTimeHours( self ):
		now = datetime.now()
		t = now.strftime( "%H:%M" )
		return t
#-------------------------------------------------------	
	def getTimeMax( self ):
		return self.timeMax
		
	def getTimeMin( self) :
		return self.timeMin
		
	def getDayMax( self ):
		return self.mDayMax
		
	def getDayMin( self) :
		return self.mDayMin
		
	def getMinimum( self ):
		self.min = 100000.0;
		x = 0
		for item in self.mData:
			if item < self.min:
				self.min = item
				self.timeMin = self.mDate[x]
				self.mDayMin = self.mDay[x]
			x += 1
		return self.min - self.mEpsilon

	def getMaximum( self ):
		self.max = -100000.0;
		x = 0
		for item in self.mData:
			if item > self.max:
				self.max = item
				self.timeMax = self.mDate[x]
				self.mDayMax = self.mDay[x]
			x += 1
		return self.max + self.mEpsilon

	def getAverage( self, data ):
		average = 0.0
		if len( data ) > 1:
			for item in data:
				average += item
			average = average / len( data)
		else:
			average = data[0]
		return average

	def printHeader( self, aName ):
		width = str( self.mWidth )
		height = str( self.mHeight )
		self.d = open( aName, "w" )
		self.d.write( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" );
		self.d.write( "<svg viewBox=\"0 0 " + width + " " + height + "\" width=\"" + width + "\" height=\"" + height + "\" xmlns=\"http://www.w3.org/2000/svg\">\n" );
		self.d.write( "<rect width=\"" + width + "\" height=\"" + height + "\" x=\"0\" y=\"0\" rx=\"0\" stroke=\"white\" fill=\"hsla(0,0%,14%,1.0)\" stroke-width=\"1\"/>\n" );

	def fillData( self, aData ):
		day = datetime.today().strftime( '%A ' )
		
		if len( self.mData ) >= self.mBufferSize:
			del self.mData[0]
			del self.mDate[0]
			del self.mDay[0]
		self.mData.append( aData )
		self.mDay.append( day )
		self.mDate.append( self.getTimeHours() )

	def getLength( self ):
		return len( self.mData )
		
	def getCurrent( self ):
		if len( self.mData ) > 0:
			return self.mData[ -1 ]
		return 0
		
	def getCurrent( self ):
		if len( self.mData ) > 0:
			return self.mData[ -1 ]
		return 0

	def renderImages( self, aModule ):
		self.printHeader( aModule[0].getEspDataT24().getName() )
		self.dataMin = 100000.0
		self.dataMax = - 100000.0
		self.currentTime = aModule[0].getEspDataT24().getCurrentTime()
		self.startTime = aModule[0].getEspDataT24().getStartTime()
		for item in aModule:
			if item.getEspDataT24().getNumberOfDataPoints() > 0:
				if item.getEspDataT24().getMinimum() < self.dataMin:
					self.dataMin = item.getEspDataT24().getMinimum()
				if item.getEspDataT24().getMaximum() > self.dataMax:
					self.dataMax = item.getEspDataT24().getMaximum()
		if self.mTrace == True:
			print( "Minimum Data=", self.dataMin )
			print( "Maximum Data=", self.dataMax )									
		for item in aModule:					
			self.renderImage( True, item.getColor(), item.getEspDataT24(), self.dataMin, self.dataMax )
		self.printFooter( self.dataMin, self.dataMax, self.startTime, self.currentTime )
		
		self.printHeader( aModule[0].getEspDataH24().getName() )
		self.dataMin = 100000.0
		self.dataMax = - 100000.0
		self.currentTime = aModule[0].getEspDataH24().getCurrentTime()
		self.startTime = aModule[0].getEspDataH24().getStartTime()
		for item in aModule:
			if item.getEspDataH24().getNumberOfDataPoints() > 0:
				if item.getEspDataH24().getMinimum() < self.dataMin:
					self.dataMin = item.getEspDataH24().getMinimum()
				if item.getEspDataH24().getMaximum() > self.dataMax:
					self.dataMax = item.getEspDataH24().getMaximum()
		if self.mTrace == True:
			print( "Minimum Data=", self.dataMin )
			print( "Maximum Data=", self.dataMax )
		for item in aModule:
			self.renderImage( True, item.getColor(), item.getEspDataH24(), self.dataMin, self.dataMax )
		self.printFooter( self.dataMin, self.dataMax, self.startTime, self.currentTime )
		
	def renderImage( self, aAverage, aColor, aEspData, aDataMin, aDataMax, aWithLimitLines=True ):
		if aEspData.getNumberOfDataPoints() < 2 :
			return
		self.hSize = self.mActiveWidth - self.mXmargin
		self.vSize = self.mActiveHeight - self.mYmargin
		# self.dataMin = aEspData.getMinimum()
		# self.dataMax = aEspData.getMaximum()
		
		self.dataMin = aDataMin
		self.dataMax = aDataMax
	
		self.amplitude = self.dataMax - self.dataMin
		self.hmult = float( self.hSize ) / float( aEspData.getNumberOfDataPoints()- 1.0 )
		self.vOffset = float( self.vSize ) + self.vSize * self.dataMin / self.amplitude
		self.vmult = self.vSize / self.amplitude

		if self.mTrace:
			print( "\n=======================================" );
			print( "Minimum Data=", self.dataMin )
			print( "Maximum Data=", self.dataMax )
			print( "Amplitude Vertikal=", self.amplitude )
			print( "Verstaerkung Vertikal=", self.vmult )
			print( "Verstaerkung Horizontal=", self.hmult )
			print( "Offset Vertikal=", self.vOffset )
			print( "\n=======================================" );

		self.d.write( "<polyline points=\"" );
		self.x = 0;
		oldPosX = 0.0
		ydata=[]
		for item in aEspData.getData():
			self.value = item * self.vmult
			self.pos = self.x * self.hmult;
			self.x += 1
			posX = self.pos + self.mXmargin / 2
			posY = ( ( self.mYmargin / 2.0 ) + self.vOffset  ) - self.value
			ydata.append( posY )
			posX = round( posX );
			posY = round( posY )
			self.mLastData = item
			if posX != oldPosX:
				oldPosX = posX;
				avgPosY = self.getAverage( ydata )
				avgPosY = round( avgPosY )
				if aAverage:
					self.d.write( str( posX ) + "," + str( avgPosY ) + " " );   # With Y average calculation
				else:
					self.d.write( str( posX ) + "," + str( posY ) + " " );  # No Y average calculation
				del ydata[:]

			# Alternative Y-Position calculation
			# self.d.write( str( posX ) + "," + str( posY ) + " " );
			# self.d.write( str( self.pos + self.mXmargin / 2 ) + "," + str( ( ( self.mYmargin / 2.0 ) + self.vOffset  ) - self.value ) + " " );

		# Letzte Y-Position fuer Anzeige aktueller Wert merken
		self.mLastY = posY

		# Abschluss polyline
		self.d.write( "\" " );
		# Style polyline
		self.d.write( "stroke-linecap=\"round\" fill=\"none\" stroke=\"" + aColor + "\" stroke-width=\"1\"/>\n" )

		if aWithLimitLines:
			# Obere Position der Limitlines bestimmen
			self.value = self.dataMax * self.vmult
			posY = ( ( self.mYmargin / 2.0 ) + self.vOffset  ) - self.value
			posY1 = str( posY )

			# Untere Position der Limitlines bestimmen
			self.value = self.dataMin * self.vmult
			posY = ( ( self.mYmargin / 2.0 ) + self.vOffset  ) - self.value
			posY2 = str( posY )

			# Start- und Endpositionen in X
			posX1 = "1"
			posX2 = str( self.mActiveWidth -1 )

			# Style Min/Max Grenzen
			style = "\" style=\"stroke:rgb(150,150,150);stroke-width:1;stroke-dasharray:5,5\""
			self.d.write( "<line x1=\"" + posX1 + "\" y1=\"" + posY1 + "\" x2=\"" + posX2 + "\" y2=\"" + posY1 + style + "/>" )
			self.d.write( "<line x1=\"" + posX1 + "\" y1=\"" + posY2 + "\" x2=\"" + posX2 + "\" y2=\"" + posY2 + style + "/>\n" )

		if self.mTrace:
			print( "\n=======================================" );
			print( "Laenge= ", aEspData.getNumberOfDataPoints() )
			print( "Name= ", self.mName )
			print( "\n=======================================" );

	def printFooter( self, aDataMin, aDataMax, aStartTime, aCurrentTime ):
		max = "%.2f" % ( aDataMax - self.mEpsilon )
		min = "%.2f" % ( aDataMin + self.mEpsilon )
		posX2 = str( self.mActiveWidth - 55 )
		# lastValue = "%.2f" % aEspData.getCurrentData()

		posY1 = str( 19 )
		posY2 = str( self.mActiveHeight - 8 )
		posY3 = str( self.mLastY + 5 )
		posY4 = str( self.mHeight - 8 )

		self.d.write( "<text>\n" );
		self.d.write( "<tspan x=\"4\" y=\"" + posY1 + "\" fill=\"white\">" + max + self.mDim + "</tspan>\n" )
		self.d.write( "<tspan x=\"4\" y=\"" + posY2 + "\" fill=\"white\">" + min + self.mDim + "</tspan>\n" )
		# self.d.write( "<tspan x=\"" + posX2 + "\" y=\"" + posY3 + "\" fill=\"white\">" + lastValue + self.mDim + "</tspan>\n" )
		self.d.write( "<tspan x=\"4\" y=\"" + posY4 + "\" fill=\"white\">" + aStartTime + "</tspan>\n" )
		self.d.write( "<tspan x=\"" + posX2 + "\" y=\"" + posY4 + "\" fill=\"white\">" + aCurrentTime + "</tspan>\n" )
		self.d.write( "</text>\n" );

		self.d.write( "</svg>\n" )
		self.d.close()

