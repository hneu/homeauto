import os, math, subprocess, codecs, time
from datetime import datetime

#-------------------------------------------------------------------------------------

def getTimeStamp( aTime ):
	if aTime == 0:
		return "-"
	now = datetime.fromtimestamp( aTime)
	return now.strftime( "%A %H:%M" )
	
#-------------------------------------------------------------------------------------
	
def getTimeStampShort( aTime ):
	if aTime == 0:
		return "-"
	now = datetime.fromtimestamp( aTime)
	
	day = now.strftime( "%A" )[0:2]
	t = day.upper() +  now.strftime( " %H:%M" )
	return t
	 
#///////////////////////////////////////////////////////////////////////////////////////

class CModuleData( object ):
	def __init__(self ):
		self.mName = []
		self.mVcc = []
		self.mLastUpdates = []
		self.mNumberOfUpdates = []
		self.mStartTime = []
		self.mIpAddress = []

#-------------------------------------------------------------------------------------

	def onUpdate( self, aName, aVcc, aLastUpdates, aNumberOfUpdates, aStartTime, aIpAddress ):
		self.mName = aName
		self.mVcc = aVcc
		self.mLastUpdates = aLastUpdates
		self.mNumberOfUpdates = aNumberOfUpdates
		self.mStartTime = aStartTime
		self.mIpAddress = aIpAddress
	
#///////////////////////////////////////////////////////////////////////////////////////
	
class CTableData( object ):
	def __init__( self ):
		self.mNumberOfUnits = []
		self.mCurrentData = []
		self.mMaximumData = []
		self.mMinimumData = []
		
	def onUpdate( self, aNumberOfUnits, aCurrentData, aMaximumData, aMinimumData ):
		self.mNumberOfUnits = aNumberOfUnits
		self.mCurrentData = aCurrentData
		self.mMaximumData = aMaximumData
		self.mMinimumData = aMinimumData
		
#///////////////////////////////////////////////////////////////////////////////////////

class CHeating( object ):

	def __init__( self, aTarget, aNavigation, aSvg1Name, aSvg1Desc, aSvg2Name, aSvg2Desc, aSvg3Name, aSvg3Desc ):
		self.mPage = aTarget + ".txt"
		self.mTarget = aTarget
		self.mSvg1Name = aSvg1Name
		self.mSvg1Desc = aSvg1Desc
		self.mSvg2Name = aSvg2Name
		self.mSvg2Desc = aSvg3Desc
		self.mSvg3Name = aSvg3Name
		self.mSvg3Desc = aSvg3Desc
		self.mNavigation = aNavigation
		self.tableTitles = ["Vorlauf HK", "Rücklauf HK", "Vorlauf FB", "Rücklauf FB", "Vorlauf WW", "Rücklauf WW"]
		self.mTTableData = CTableData()
		self.mHTableData = CTableData()
		self.mMTableData = CModuleData()

#-------------------------------------------------------------------------------------
		
	def updateMTableData( self, aName, aVcc, aLastUpdates, aNumberOfUpdates, aStartTime, aIpAddress ):
		self.mMTableData.onUpdate( aName, aVcc, aLastUpdates, aNumberOfUpdates, aStartTime, aIpAddress )
		
#-------------------------------------------------------------------------------------

	def updateTTableData( self, aNumberOfUnits, aCurrentData, aMaximumData, aMinimumData ):
		self.mTTableData.onUpdate( aNumberOfUnits, aCurrentData, aMaximumData, aMinimumData )
		
#-------------------------------------------------------------------------------------

	def updateHTableData( self, aNumberOfUnits, aCurrentData, aMaximumData, aMinimumData ):
		self.mHTableData.onUpdate( aNumberOfUnits, aCurrentData, aMaximumData, aMinimumData )
		
#-------------------------------------------------------------------------------------
	def pageHeader( self ):
		self.d.write( "<!DOCTYPE html>\n" )
		self.d.write( "<html>\n" )
		self.d.write( "  <head>\n" )
		self.d.write( "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\n" )
		self.d.write( "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" )
		self.d.write( "    <meta http-equiv=\"Cache-Control\" content=\"no-cache, no-store, must-revalidate\"/>\n" )
		self.d.write( "    <link rel=\"stylesheet\" href=\"stylesheet.css\">\n" )
		self.d.write( "  </head>\n" )

#-------------------------------------------------------------------------------------

	def pageNavigation( self ):
		self.d.write( "  <ul class=\"sidenav\">\n" )
		
		for item in self.mNavigation:
			self.d.write( item )
	
		self.d.write( "  </ul>\n" )

#-------------------------------------------------------------------------------------

	def pageGallery( self ):
		self.d.write( "  <div class=\"content\">\n" )
		self.d.write( "    <div class=\"spacer\"></div>\n" )
		self.d.write( "    <div class=\"gallery\">\n" )
		self.d.write( "      <a target=\"\" href=\"" + self.mSvg1Name + "\">\n" )
		self.d.write( "        <img src=\"" + self.mSvg1Name + "\">\n" )
		self.d.write( "      </a>\n" )
		self.d.write( "      <div class=\"desc\">" + self.mSvg1Desc + "</div>\n" )
		self.d.write( "    </div>\n" )
		self.d.write( "    <div class=\"gallery\">\n" )
		self.d.write( "      <a target=\"\" href=\"" + self.mSvg2Name + "\">\n" )
		self.d.write( "        <img src=\"" + self.mSvg2Name + "\">\n" )
		self.d.write( "      </a>\n" )
		self.d.write( "      <div class=\"desc\">" + self.mSvg2Desc + "</div>\n" )
		self.d.write( "    </div>\n" )
		self.d.write( "    <div class=\"gallery\">\n" )
		self.d.write( "      <a target=\"\" href=\"" + self.mSvg3Name + "\">\n" )
		self.d.write( "        <img src=\"" + self.mSvg3Name + "\">\n" )
		self.d.write( "      </a>\n" )
		self.d.write( "      <div class=\"desc\">" + self.mSvg3Desc + "</div>\n" )
		self.d.write( "    </div>\n" )
		self.d.write( "  </div>\n" )
	
		self.d.write( "  <div class=\"content\">\n" )
		self.d.write( "    <div class=\"spacer\"></div>\n" )
		self.d.write( "  </div>\n" )

#-------------------------------------------------------------------------------------
		
	def pageBody( self, open=True ):
		if open:
			self.d.write( "  <body>\n" )
		else:
			self.d.write( "  <\body>\n" )

#-------------------------------------------------------------------------------------
	def tableHeader( self, aCol1, aCol2, aCol3, aCol4, aCol5, aCol6, aCol7 ):
		self.d.write( "<table>\n" )
		self.d.write( "  <tr>\n" )
		self.d.write( "    <th>" + aCol1 + "</th>\n" )
		self.d.write( "    <th>" + aCol2 + "</th>\n" )
		self.d.write( "    <th>" + aCol3 + "</th>\n" )
		self.d.write( "    <th>" + aCol4 + "</th>\n" )
		self.d.write( "    <th>" + aCol5 + "</th>\n" )
		self.d.write( "    <th>" + aCol6 + "</th>\n" )
		self.d.write( "    <th>" + aCol7 + "</th>\n" )
		self.d.write( "  </tr>\n" )
		
#-------------------------------------------------------------------------------------
		
	def tableRow( self, aCol1, aCol2, aCol3, aCol4, aCol5, aCol6, aCol7, end=False ):
		self.d.write( "  <tr>\n" )
		self.d.write( "    <td>" + aCol1 + "</td>\n" )
		self.d.write( "    <td>" + aCol2 + "</td>\n" )
		self.d.write( "    <td>" + aCol3 + "</td>\n" )
		self.d.write( "    <td>" + aCol4 + "</td>\n" )
		self.d.write( "    <td>" + aCol5 + "</td>\n" )
		self.d.write( "    <td>" + aCol6 + "</td>\n" )
		self.d.write( "    <td>" + aCol7 + "</td>\n" )
		self.d.write( "  </tr>\n" )
		if end:
			self.d.write( "</table>\n" )

#-------------------------------------------------------------------------------------
			
	def spacer( self ):
		self.d.write( "<div class=\"spacer\"></div>\n")

#-------------------------------------------------------------------------------------

	def openTable( self ):
		self.d = codecs.open( self.mPage, "w", "utf-8" )
		self.pageHeader()
		self.pageBody()
		self.pageNavigation()
		self.pageGallery()
		
#-------------------------------------------------------------------------------------
		
	def closeTable( self ):
		self.footer()
		self.d.close()
		subprocess.call( "cp " + self.mPage + " " + self.mTarget, shell=True )
#-------------------------------------------------------------------------------------

	#n Die Backend Daten werdenn aus dem Übergabebereich für diese Tabelle gelesen und in die entsprechende Tabelle in HTML eingebettet
	def updateTTable( self, aLink ):
		datapoints = []
		for item in self.mTTableData.mNumberOfUnits:
			datapoints.append( "%d" % item )
			
		currents = []
		for item in self.mTTableData.mCurrentData:
			currents.append( "<font color=red> %05.2f" % item + "&#176;</font>" )
			
		max = []
		for item in self.mTTableData.mMaximumData:
			max.append( "<font color=red> %05.2f" % item + "&#176;</font>&nbsp;&nbsp;&#128336;&nbsp;" )
			
		min = []
		for item in self.mTTableData.mMinimumData:
			min.append( "<font color=red> %05.2f" % item + "&#176;</font>&nbsp;&nbsp;&#128336;&nbsp;" )
		
		link = [ aLink + "-1.svg", aLink + "-2.svg", aLink + "-3.svg", aLink + "-4.svg", aLink + "-5.svg", aLink + "-6.svg" ]
		
		header = []
		for item in self.tableTitles:
			header.append( "<font color=white>" + item + "</font>" )
		
		details = []
		details.append( "<font color=red><b><i>Temperatur</i></b></font>" )
		for index in range( 0, 6 ):
			details.append( "<a href=\"" + link[index] + "\">Details</a>" )
		
		self.d.write( "<div class=\"content\">\n" )
		self.tableHeader( "",                header[0],     header[1],     header[2],     header[3],     header[4],     header[5] )
		self.tableRow(  details[0],          details[1],    details[2],    details[3],    details[4],    details[5],    details[6] )
		self.tableRow( "Aktueller Messwert", currents[0],   currents[1],   currents[2],   currents[3],   currents[4],   currents[5] )
		self.tableRow( "Maximalwert",        max[0],        max[1],        max[2],        max[3],        max[4],        max[5] )
		self.tableRow( "Minimalwert",        min[0],        min[1],        min[2],        min[3],        min[4],        min[5] )
		self.tableRow( "Stützpunkte",        datapoints[0], datapoints[1], datapoints[2], datapoints[3], datapoints[4], datapoints[5] ) 

#-------------------------------------------------------------------------------------

	# Die Backend Daten werdenn aus dem Übergabebereich für diese Tabelle gelesen und in die entsprechende Tabelle in HTML eingebettet
	def updateMTable( self ):
		name = self.mMTableData.mName
		startTime = self.mMTableData.mStartTime
		vcc = []
		lastUpdates = []
		updates = []
		numberOfUpdates = self.mMTableData.mNumberOfUpdates
		ipAddress = self.mMTableData.mIpAddress
		
		for item in self.mMTableData.mVcc:
			vcc.append( "%04.2f" % item + " Volt" )
			
		for item in self.mMTableData.mLastUpdates:
			lastUpdates.append( getTimeStamp( item ) )
		
		for item in self.mMTableData.mNumberOfUpdates:
			updates.append( "%d" % item )
	
		self.tableRow( "<font color=red><b><i>Modul</i></b></font>", name[0], name[1], name[2], name[3], name[4], name[5] )
		self.tableRow( "IP-Adresse", ipAddress[0], ipAddress[1], ipAddress[2], ipAddress[3], ipAddress[4], ipAddress[5] )
		self.tableRow( "Start", startTime[0], startTime[1], startTime[2], startTime[3], startTime[4], startTime[5] )
		self.tableRow( "Versorgungsspannung", vcc[0], vcc[1], vcc[2], vcc[3], vcc[4], vcc[5] )
		self.tableRow( "Letzes Update", lastUpdates[0], lastUpdates[1], lastUpdates[2], lastUpdates[3], lastUpdates[4], lastUpdates[5] )
		self.tableRow( "Anzahl Updates", updates[0], updates[1], updates[2], updates[3], updates[4], updates[5], True )
		
		self.d.write( "    <div class=\"spacer\"></div>\n" )
		self.d.write( "    </div>\n" )
		
#-------------------------------------------------------------------------------------
		
	def footer( self ):
		now = datetime.now()
		t = now.strftime( "am %D um %H:%M:%S" )

		self.d.write( "    <div class=\"content\">\n" )
		self.d.write( "      <div class=\"spacer\"></div>\n" )
		self.d.write( "      <h3>Letzte Aktualisierung " + t + "</h3>\n" )
		self.d.write( "    </div>\n" )
		self.d.write( "  </body>\n" )
		self.d.write( "</html>\n" )
	
	
