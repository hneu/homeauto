import os, math
from datetime import datetime
from espdata import CEspData
from module import CModule
from unitContainer import CValuesUnit

#-------------------------------------------------------------------------------------

# Ausgabe Zeit in der Form Stunde, Minute
def getTimeHM( aTimeStamp ):
	ta = datetime.fromtimestamp( aTimeStamp )
	t = ta.strftime( "%H:%M" )
	return t

#-------------------------------------------------------------------------------------

# Ausgabe Zeit in der Form Wochentag[0:2], Stunde, Minute
def getTimeDsHM( aTimeStamp ):
	ta = datetime.fromtimestamp( aTimeStamp )
	t = ta.strftime( "%A" )[0:2] + ta.strftime( " %H:%M" )
	return t

#-------------------------------------------------------------------------------------

# Ausgabe Zeit in der Form: Wochentag, Stunde, Minute
def getTimeDHM( aTimeStamp ):
	ta = datetime.fromtimestamp( aTimeStamp )
	t = ta.strftime( "%A %H:%M" )
	return t

#-------------------------------------------------------------------------------------

# Ausgabe Datum in der Form: Wochentag, Tag, Monat Jahr
def getTimeDmY( aTimeStamp ):
	ta = datetime.fromtimestamp( aTimeStamp )
	t = ta.strftime( "%A %d.%m.%Y" )
	return t

#-------------------------------------------------------------------------------------

def getAverage( data ):
	average = 0.0
	if len( data ) > 1:
		for item in data:
			average += item
		average = average / len( data )
	else:
		average = data[0]
	return average

#-------------------------------------------------------------------------------------

def getSvgLine( aX1, aX2, aY1, aY2, aStyle ):
	svgStyle1 = "style=\"stroke:rgb(150,0,0);stroke-width:1\" "
	svgStyle2 = "style=\"stroke:rgb(150,150,150);stroke-width:1\" "
	svgStyle3 = "style=\"stroke:rgb(150,150,150);stroke-width:1;stroke-dasharray:5,5\" "
	svgStyle4 = "style=\"stroke:rgb(255,0,0);stroke-width:1\" "
	
	open = "<line "
	close = "/>\n"
	
	svgX1 = "x1=\"" + str( aX1 ) + "\" "
	svgX2 = "x2=\"" + str( aX2 ) + "\" "
	svgY1 = "y1=\"" + str( aY1 ) + "\" "
	svgY2 = "y2=\"" + str( aY2 ) + "\" "
	svgStyle = svgStyle1
	
	if aStyle == 1:
		svgStyle = svgStyle1
	elif aStyle == 2:
		svgStyle = svgStyle2
	elif aStyle == 3:
		svgStyle = svgStyle3
	elif aStyle == 4:
		svgStyle = svgStyle4

	svgLine = open + svgX1 + svgY1 + svgX2 + svgY2 + svgStyle + close
	return svgLine

#-------------------------------------------------------------------------------------

def getSvgText( aX, aY, aColor, aText, aSize = 0.9 ):
	size = "font-size=" + "\"" + ( "%.2f" % aSize ) + "em\""
	open = "<text "
	close = "</text>\n"
	svgX = "x=\"" + str( aX ) + "\" "
	svgY = "y=\"" + str( aY ) + "\" "
	color = "fill=\"" + aColor + "\"" + " font-family=\"sans-serif\" " + size + ">"
	
	svgText = open + svgX + svgY + color + aText + close
	return svgText

#///////////////////////////////////////////////////////////////////////////////////////

class CPolyLine( object ):

	def __init__( self, aColor ):
		self.mIsOpen = False
		self.mColor = aColor
		self.mInitialColor = aColor
		self.mMode = 0;
	
	def open( self, aFile, aMode ):
		if self.mMode == 0:
			aFile.write( "<polyline points=\"" );
			self.mMode = aMode
			return
		
		if aMode != self.mMode:
			self.close( aFile )
			if aMode == 2:
				self.mColor = "white"
			else:
				self.mColor = self.mInitialColor
			aFile.write( "<polyline points=\"" );
			self.mMode = aMode
			
	def close( self, aFile ):
		if self.mMode != 0:
			self.mMode = 0
			aFile.write( "\" " );
			aFile.write( "stroke-linecap=\"round\" fill=\"none\" stroke=\"" + self.mColor + "\" stroke-width=\"1\"/>\n" )
	

#///////////////////////////////////////////////////////////////////////////////////////

class CSvgRender( object ):

	def __init__( self,  aWidth, aHeight, aPath = "images" ):
		self.mWidth = aWidth
		self.mHeight = aHeight
		self.mPath = aPath
		self.mTrace = False
		self.mEpsilon = 0.001
		self.mLastY = 0;
		self.mLastData = 0
		self.mStartDataX = 70

		self.mActiveWidth = aWidth - 30
		if self.mActiveWidth < 0:
			self.mActiveWidth = aWidth

		self.mActiveHeight = aHeight - 65

		self.mYmargin = 50
		self.mXmargin = 30

#-------------------------------------------------------------------------------------

	def printHeader( self, aName ):
		width = str( self.mWidth )
		height = str( self.mHeight )
		self.d = open( self.mPath + "/" + aName, "w" )
		self.d.write( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" );
		self.d.write( "<svg viewBox=\"0 0 " + width + " " + height + "\" width=\"" + width + "\" height=\"" + height + "\" xmlns=\"http://www.w3.org/2000/svg\">\n" )
		self.d.write( "<rect width=\"" + width + "\" height=\"" + height + "\" x=\"0\" y=\"0\" rx=\"0\" stroke=\"white\" fill=\"hsla(0,0%,14%,1.0)\" stroke-width=\"1\"/>\n" )

#-------------------------------------------------------------------------------------

	def renderImage( self, aName, aAverage, aColor, aValuesUnit, aIndex, aDataMin, aDataMax, aDim, aStartTime, aLastTime, aCurrentData, aWithLimitLines=True ):
		self.printHeader( aName )
		self.render( aAverage, aColor, aValuesUnit, aIndex, aDataMin, aDataMax, aWithLimitLines )
		self.printFooter( aDataMin, aDataMax, aStartTime, aLastTime, aCurrentData, aDim, aWithLastValue=True )

#-------------------------------------------------------------------------------------

	def render( self, aAverage, aColor, aValuesUnit, aIndex, aDataMin, aDataMax, aWithLimitLines=True ):
		polyLine = CPolyLine( aColor )
		unitLength = len( aValuesUnit )
		if unitLength < 2:
			return
		
		self.hSize = self.mActiveWidth - self.mXmargin - self.mStartDataX
		self.vSize = self.mActiveHeight - self.mYmargin
		
		self.dataMin = aDataMin - self.mEpsilon
		self.dataMax = aDataMax + self.mEpsilon
	
		self.amplitude = self.dataMax - self.dataMin
		self.hmult = float( self.hSize ) / float( unitLength - 1.0 )
		self.vOffset = float( self.vSize ) + self.vSize * self.dataMin / self.amplitude
		self.vmult = self.vSize / self.amplitude
		
		# self.d.write( "<polyline points=\"" );
		self.x = 0;
		oldPosX = 0.0
		ydata=[]
		x = 0
		for item in aValuesUnit:
			value = item.getValues()[aIndex]
			valid = item.getValidMask()[aIndex]
			if valid == 1:
				# print( "open polyLine for x = ", x )
				polyLine.open( self.d, 1 )
			elif valid == 2:
				# print( "open polyLine for x = ", x )
				# polyLine.close( self.d )
				polyLine.open( self.d, 1 )
			elif valid == 0:
				polyLine.close( self.d )
				del ydata[:]
			x = x + 1
			self.value = value * self.vmult
			self.pos = self.x * self.hmult;
			self.x += 1
			posX = self.mStartDataX  + self.pos
			posY = ( ( self.mYmargin / 2.0 ) + self.vOffset  ) - self.value
			ydata.append( posY )
			posY = round( posY )
				
			posX = round( posX );
			self.mLastData = item
			if posX != oldPosX:
				oldPosX = posX
				avgPosY = getAverage( ydata )
				avgPosY = round( avgPosY )
					
				if valid:
					if aAverage:
						self.d.write( str( posX ) + "," + str( avgPosY ) + " " );   # With Y average calculation
					else:
						self.d.write( str( posX ) + "," + str( posY ) + " " );  # No Y average calculation
				del ydata[:]

			# Alternative Y-Position calculation
			# self.d.write( str( posX ) + "," + str( posY ) + " " );
			# self.d.write( str( self.pos + self.mXmargin / 2 ) + "," + str( ( ( self.mYmargin / 2.0 ) + self.vOffset  ) - self.value ) + " " );

		# Letzte Y-Position fuer Anzeige aktueller Wert merken
		self.mLastY = posY

		polyLine.close( self.d )
		# Abschluss polyline
		#self.d.write( "\" " );
		# Style polyline
		# self.d.write( "stroke-linecap=\"round\" fill=\"none\" stroke=\"" + aColor + "\" stroke-width=\"1\"/>\n" )
		
		if aWithLimitLines:
			# Obere Position der Limitlines bestimmen
			self.value = self.dataMax * self.vmult
			y1 = ( ( self.mYmargin / 2.0 ) + self.vOffset  ) - self.value

			# Untere Position der Limitlines bestimmen
			self.value = self.dataMin * self.vmult
			y2 = ( ( self.mYmargin / 2.0 ) + self.vOffset  ) - self.value

			# Start- und Endpositionen in X
			x1 = self.mStartDataX - 10
			x2 = self.mActiveWidth - 25

			# Style Min/Max Grenzen
			self.d.write( getSvgLine( x1, x2,  y1, y1, 3 ) )
			self.d.write( getSvgLine( x1, x2,  y2, y2, 3 ) )

#-------------------------------------------------------------------------------------

	def printFooter( self, aDataMin, aDataMax, aStartTime, aCurrentTime, aCurrentData, aDim, aWithLastValue ):

		start = getTimeHM( aStartTime )
		current = getTimeHM( aCurrentTime )
		startDate = getTimeDmY( aStartTime)
		delta24 = ( aCurrentTime - aStartTime ) / 24
		
		max = "%.2f" % ( aDataMax )
		min = "%.2f" % ( aDataMin )
		posX2 = str( self.mActiveWidth - 42 )
		posX3 = str( self.mActiveWidth - 25 )
		lastValue = "%.2f" % aCurrentData
		yPosTimeScale = self.mHeight - 45
		yPosTimeValue = yPosTimeScale + 17
		yPosDateValue = yPosTimeScale + 37

		posY1 = str( 19 ) # Position Maximalwert Anzeige
		posY2 = str( self.mActiveHeight - 8 ) # Position Minimalwert Anzeige
		posY3 = str( self.mLastY + 5 )
		posY4 = str( self.mHeight - 8 )
		
		# printing y-axis midline
		svgMidLine = getSvgLine( self.mStartDataX - 10, self.mActiveWidth - 25, self.mActiveHeight / 2, self.mActiveHeight / 2, 3 )
		self.d.write( svgMidLine )
		
		# printing y-axis scalelines and scales
		ydist = self.mActiveHeight / 8
		yval = ( aDataMax - aDataMin ) / 8.0
		x1 = self.mStartDataX - 10
		x2 = self.mStartDataX - 0
		for x in range( 1, 4 ):
			line = getSvgLine( x1, x2, self.mActiveHeight - x * ydist, self.mActiveHeight - x * ydist, 2 )
			self.d.write( line )
			
		for x in range( 1, 4 ):
			line = getSvgLine( x1, x2,  x * ydist, x * ydist, 2 )
			self.d.write( line )
			
		for x in range( 1, 8 ):
			line = getSvgText( 5, ( x * ydist ) + 4, "white", "%.2f" % (  aDataMax - yval * x ) + aDim )
			self.d.write( line )
		
		# printing min and max values
		self.d.write( getSvgText( self.mStartDataX - 10,     posY1, "red", max + aDim ) )
		self.d.write( getSvgText( self.mStartDataX - 10,     posY2, "red", min + aDim ) )

		
		# printing time scale
		# dividing the scale line into 12 major and into 12 minor lines for scale it into 24 points
		deltaX = ( ( self.mActiveWidth - 25 ) - ( self.mStartDataX - 10 ) ) / 24
		x1 = self.mStartDataX - 10
		y1 = yPosTimeScale
		y2 = yPosTimeScale - 15

		# Untere Linie Zeitenskala
		line = getSvgLine( 0, self.mWidth,  yPosTimeScale, yPosTimeScale, 2 )
		self.d.write( line )
		
		# Startdatum unterste Reihe eintragen
		self.d.write( getSvgText( x1 - 14, yPosDateValue, "white", startDate, 0.72 ) )

		# Alternierende Zeitlinien und Zeiten eintragen
		even = True
		for x in range( 0, 25 ):
			xp = x1 + x * deltaX
			line = getSvgLine( xp, xp, y1, y2, 2 )
			self.d.write( line )
			if even == True:
				now = getTimeHM( aStartTime + delta24 * x )
				self.d.write( getSvgText( xp - 14, yPosTimeValue, "white", now, 0.72 ) )
				y2 = yPosTimeScale - 10
				even = False
			else:
				y2 = yPosTimeScale - 15
				even = True

		# printing current value when enabled
		if aWithLastValue:
			self.d.write( getSvgText( posX3, posY3, "white", lastValue + aDim ) )
			
		self.d.write( "</svg>\n" )
		self.d.close()

#-------------------------------------------------------------------------------------

