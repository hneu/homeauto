#!/usr/bin/python
# -*- coding: utf-8 -*-
# Version 1.0.0

import time

#///////////////////////////////////////////////////////////////////////////////////////

class CValuesUnit( object ):
	def __init__( self ):
		self.mTimeStamp = time.time()
		self.mValidMask = [0] * 20
		self.mValues = [0.0] * 20
		self.mValidIndexes = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 ]
		
#-------------------------------------------------------------------------------------

	def getTimeStamp( self ):
		return self.mTimeStamp

#-------------------------------------------------------------------------------------
		
	def setValidMask( self, aValidMask ):
		self.mValidMask = aValidMask

#-------------------------------------------------------------------------------------
		
	def getValidMask( self ):
		return self.mValidMask

#-------------------------------------------------------------------------------------

	def setValues( self, aValues ):
		self.mValues = aValues

#-------------------------------------------------------------------------------------

	def getValues( self ):
		return self.mValues
		
#-------------------------------------------------------------------------------------

	def getValidIndexes( self ):
		return self.mValidIndexes
		
#-------------------------------------------------------------------------------------

	def getMinimum( self ):
		localMin = 1000
		for valid, value in zip( self.mValidMask, self.mValues ):
			if valid == 1:
				if value < localMin:
					localMin = value
		return localMin
			
#-------------------------------------------------------------------------------------

	def getMaximum( self ):
		localMax = -1000
		for valid, value in zip( self.mValidMask, self.mValues ):
			if valid == 1:
				if value > localMax:
					localMax = value
		return localMax
		
#-------------------------------------------------------------------------------------

#///////////////////////////////////////////////////////////////////////////////////////

class CUnitContainer( object ):
	def __init__( self, aBufferSize ):
		self.mBufferSize = aBufferSize
		self.mContainer = []

#-------------------------------------------------------------------------------------

	def appendUnit( self, aUnit ):
		if len( self.mContainer ) >= self.mBufferSize:
			del self.mContainer[0]
		self.mContainer.append( aUnit )

#-------------------------------------------------------------------------------------

	def mergeLastUnit( self, aUnit ):
		mergeMask = self.getLastUnit().getValidMask()
		targetMask = aUnit.getValidMask()
		targetValues = aUnit.getValues()
		mergeValues = self.getLastUnit().getValues()
		for index in range( 0, len( targetMask ) - 1 ):
			if targetMask[index] == 0 and mergeMask[index] != 0:
				targetValues[index] = mergeValues[index]
				targetMask[index] = 2
		valuesUnit = CValuesUnit();
		valuesUnit.setValues( targetValues )
		valuesUnit.setValidMask( targetMask )
		self.appendUnit( valuesUnit )

#-------------------------------------------------------------------------------------

	def getUnits( self ):
		return self.mContainer

#-------------------------------------------------------------------------------------

	def getLastUnit( self ):
		unit = CValuesUnit()
		size = len( self.mContainer )
		if size > 0:
			unit = self.mContainer[size - 1]
		return unit

#-------------------------------------------------------------------------------------
		
	def getMinimum( self ):
		localMin = 1000
		for item in self.mContainer:
			if localMin > item.getMinimum():
				localMin = item.getMinimum()
		return localMin

#-------------------------------------------------------------------------------------

	def getMin4Ind( self, aIndexes ):
		localMin = 1000
		for index in aIndexes:
			indexMin = self.getMinimumAt( index )
			if localMin > indexMin:
				localMin = indexMin
		return localMin

#-------------------------------------------------------------------------------------

	def getMinimumAt( self, aIndex ):
		if aIndex < 0 or aIndex > 19:
			return -1000
		localMin = 1000
		for item in self.mContainer:
			if item.getValidMask()[aIndex] == True:
				if localMin > item.getValues()[aIndex]:
					localMin = item.getValues()[aIndex]
		return localMin
		
#-------------------------------------------------------------------------------------

	def getMinimumVectorAt( self, aIndexes ):
		localMin = []
		for item in aIndexes:
			localMin.append( self.getMinimumAt( item ) )
		return localMin
		
#-------------------------------------------------------------------------------------

	def getMaximum( self ):
		localMax = -1000
		for item in self.mContainer:
			if localMax < item.getMaximum():
				localMax = item.getMaximum()
		return localMax

	#-------------------------------------------------------------------------------------

	def getMax4Ind( self, aIndexes ):
		localMax = -1000
		for index in aIndexes:
			indexMax = self.getMaximumAt( index )
			if localMax < indexMax:
				localMax = indexMax
		return localMax

#-------------------------------------------------------------------------------------

	def getMaximumAt( self, aIndex ):
		if aIndex < 0 or aIndex > 19:
			return -1000
		localMax = -1000
		for item in self.mContainer:
			if item.getValidMask()[aIndex] == True:
				if localMax < item.getValues()[aIndex]:
					localMax = item.getValues()[aIndex]
		return localMax
		
#-------------------------------------------------------------------------------------

	def getMaximumVectorAt( self, aIndex ):
		localMax = []
		for item in aIndex:
			localMax.append( self.getMaximumAt( item ) )
		return localMax
		
#-------------------------------------------------------------------------------------

	def getNumberOfUnits( self ):
		return len( self.mContainer )

#-------------------------------------------------------------------------------------

	def getNumberOfUnitsAt( self, aIndex ):
		if aIndex < 0 or aIndex > 19:
			return 0
		count = 0;
		for item in self.mContainer:
			if item.getValidMask()[aIndex] == True:
				count += 1
		return count

#-------------------------------------------------------------------------------------

	def getNumberOfUnitsVectorAt( self, aIndex ):
		numbers = []
		for item in aIndex:
			numbers.append( self.getNumberOfUnitsAt( item ) )
		return numbers
		
#-------------------------------------------------------------------------------------

	def getStartTime( self ):
		if self.getNumberOfUnits() > 0:
			return self.mContainer[0].getTimeStamp()
		else:
			return time.time()
		
#-------------------------------------------------------------------------------------

	def getCurrentTime( self ):
		if self.getNumberOfUnits() > 0:
			return self.mContainer[self.getNumberOfUnits() - 1].getTimeStamp()
		else:
			return time.time()

#-------------------------------------------------------------------------------------	

	def getStartTimeAt( self, aIndex ):
		if self.getNumberOfUnits() > 0:
			for item in self.mContainer:
				if item.getValidMask()[aIndex] == True:
					return item.getTimeStamp()
		return time.time()

#-------------------------------------------------------------------------------------

	def getCurrentTimeAt( self, aIndex ):
		if self.getNumberOfUnits() > 0:
			for item in reversed( self.mContainer ):
				if item.getValidMask()[aIndex] == True:
					return item.getTimeStamp()
		return time.time()

#-------------------------------------------------------------------------------------

	def getCurrentDataAt( self, aIndex ):
		data = 0
		if self.getNumberOfUnits() > 0:
			if self.mContainer[self.getNumberOfUnits() - 1].getValidMask()[aIndex] == True:
				data = self.mContainer[self.getNumberOfUnits() - 1].getValues()[aIndex]
		return data	

#-------------------------------------------------------------------------------------	
		
	def getCurrentDataVectorAt( self, aIndex ):
		data = []
		for item in aIndex:
			data.append( self.getCurrentDataAt( item ) )
		return data	
			
#///////////////////////////////////////////////////////////////////////////////////////
