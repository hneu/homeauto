#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "DHT.h"

const char* moduleName = "1010";
const char* moduleCapacity = "07";
const char* SSID = "TP-FRITZ-BASE-2.4";
const char* PSK = "Fritz.001-69";
const char* MQTT_BROKER = "192.168.1.6";
const char* MQTT_CLIENT = moduleName;
bool mqttConnectionEstablished = false;

// ======= DHT Sensor =======
#define DHTTYPE DHT22
uint8_t DHTPin = 4;
DHT dht( DHTPin, DHTTYPE );
float Temperature;
float Humidity;

// ======= Versorungsspannung uberwachen =======
ADC_MODE( ADC_VCC );
float Vcc;

static int on=1;
static int counter = 100; // 10 entspricht 1 Sekunde

WiFiClient espClient;
PubSubClient client( espClient );

/**************************************************************************/

void setup() {
    Serial.begin( 115200 );
   
    setup_wifi();
    pinMode( DHTPin, INPUT );
    dht.begin();
  
    client.setServer( MQTT_BROKER, 1883 );
    client.setCallback( callback );
}

/**************************************************************************/

void setup_wifi() {
    WiFi.begin( SSID, PSK );
 
    while( WiFi.status() != WL_CONNECTED )
        delay( 100 );

    Serial.println( "WIFI connected" );
    Serial.println( WiFi.localIP() );
}

/**************************************************************************/

void getData()
{
    Temperature = dht.readTemperature();
    Humidity = dht.readHumidity();
    uint16_t vcc = ESP.getVcc();
    Vcc = ( ( float ) vcc / 1024.0f );
}

/**************************************************************************/

void publishData()
{
    String telegram = "@" + String( moduleName ) + String ( moduleCapacity ) + String ( Temperature, 2 ) + String ( Humidity, 2 ) + "0" + String( Vcc, 2 ) + WiFi.localIP().toString();
    client.publish( "/home/data", telegram.c_str() );
}

/**************************************************************************/

void loop() {
    if( !client.connected() )
    {
        while( !client.connected() )
        {
            client.connect( MQTT_CLIENT );
            client.subscribe( "/home/mtt" );
            delay( 100 );
        }
    }
    if( !mqttConnectionEstablished )
    {
        String str = MQTT_CLIENT;
        str = str + " CONNECTED";
        client.publish( "/home/data", str.c_str() );
        Serial.println( "Client connected" );
        mqttConnectionEstablished = true;
    }
    getData();
    delay( 100 );
    publishData();
    delay ( 100 );
    ESP.deepSleep( 60e6 );
    // Keine Client loop wenn vorher der deepSleep angewendet wurde, das System macht nach dem deepSleep einen Reset
    // client.loop();
}

/**************************************************************************/

void callback( char* topic, byte* payload, unsigned int length )
{
    Serial.println( "Callback gerufe" );
    String msg;
    for( byte i = 0; i < length; i++ )
    {
        char tmp = char( payload[i] );
        msg += tmp;
    }
    Serial.println( msg );
    if( msg == "HOME 1000" )
    {
        Serial.println( "Callback: ID 1000" );
        client.publish( "/home/data", "1000: Switch DHT ON" );
        on = 1;
        counter = 100;
    }
    if( msg == "HOME 1001" )
    {
        Serial.println( "Callback: ID 1001" );
        client.publish( "/home/data", "1008:Switch DHT OFF" );
        on = 0;
    }
    if( msg == "HOME 1002" )
    {
        Serial.println( "Callback: ID 1002" );
        getData();
        publishData();
        client.publish( "/home/data", "1009:Switch SUBSCRIBER OFF" );
    }
}

/**************************************************************************/
