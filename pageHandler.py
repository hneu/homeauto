#!/usr/bin/python
# -*- coding: utf-8 -*-

# Version 1.0.0
import os, math, random, sys, time, math
from datetime import datetime
from oma import CPageOma
from svgRender import CSvgRender
from climate import CClimate
from heating import CHeating
from dataHandler import CDataHandler
from unitContainer import CUnitContainer

# Module Adressen
# Vorlauf HK: 	1023
# Rücklauf HK:	1020
# Vorlauf FB:	1021
# Rücklauf FB:	1024
# Vorlauf WW:	1025
# Rücklauf WW:	1022

class CPageHandler( object ):
	
	def __init__( self, aDataHandler ):
		self.mDH = aDataHandler
		self.mNumberOfUnits = []
		self.mCurrentData = []
		self.mMaximumData = []
		self.mMinimumData = []
		
		self.mColors = ["red", "green", "blue", "pink", "white", "white", "white", "white", "white", "white", "red", "green", "blue", "pink", "white", "white", "white", "white", "white", "white"]
		
		# Zuordnung der Module von links nach rechts
		self.mClimaIndexes = [0, 1, 2, 3]
		
		# Benamung der Navigation links von links nach rechts, entspricht dem Index des Detailbildes. Der Index geht von 1 bis n.
		self.mClimImgInd = ["1", "2", "3", "4", "5", "6"]
		
		# Zuordnung der Module zur Reihenfolge von links nach rechts in der Tabelle
		self.mHeatIndexes = [13, 10, 11, 14, 15, 12]
		
		# Aufschlüsselung nach Heizkreis
		self.mHK1Indexes = [13, 10]	# Vorlauf HK, Rücklauf HK -> HK1
		self.mHK2Indexes = [11, 14]	# Vorlauf FB, Rücklauf FB -> HL2
		self.mHK3Indexes = [15, 12]	# Vorlauf WW, Rücklauf WW -> HK3
		
		# Benamung der Navigation links von links nach rechts, entspricht dem Index des Detailbildes. Der Index geht von 1 bis n
		self.mHeatImgInd = ["1", "2", "3", "4", "5", "6"]
		
		self.navOmaMain = [ "    <li><a href=\"index.html\">&#8962; Home</a></li>\n", "    <li><a class=\"active\" href=\"vital.html\">&#128147; Vital</a></li>\n", "    <li><a href=\"drives.html\">&#128436; Drives</a></li>\n" ]
		self.omaMain = CPageOma( "vital.html", self.navOmaMain )
		
		climNav1 = [ "    <li><a href=\"index.html\">&#8962; Home</a></li>\n", "    <li><a class=\"active\" href=\"climate24h.html\">&#128336; 24 Räume</a></li>\n", "    <li><a href=\"climate72h.html\">&#128336; 72 Räume</a></li>\n" ]
		climNav2 = [ "    <li><a href=\"index.html\">&#8962; Home</a></li>\n", "    <li><a href=\"climate24h.html\">&#128336; 24 Räume</a></li>\n", "    <li><a class=\"active\" href=\"climate72h.html\">&#128336; 72 Räume</a></li>\n" ]
		
		self.clim24h = CClimate ( "climate24h.html", climNav1, "images/tmp24.svg", "24 Stunden Temperatur Aufzeichnung", "images/hum24.svg", "24 Stunden Feuchte Aufzeichnung" )
		self.clim72h = CClimate ( "climate72h.html", climNav2, "images/tmp72.svg", "72 Stunden Temperatur Aufzeichnung", "images/hum72.svg", "72 Stunden Feuchte Aufzeichnung" )
		
		heatNav1 = [ "    <li><a href=\"index.html\">&#8962; Home</a></li>\n", "    <li><a class=\"active\" href=\"heating24h.html\">&#128336; 24 Heizung</a></li>\n", "    <li><a href=\"heating72h.html\">&#128336; 72 Heizung</a></li>\n" ]
		heatNav2 = [ "    <li><a href=\"index.html\">&#8962; Home</a></li>\n", "    <li><a href=\"heating24h.html\">&#128336; 24 Heizung</a></li>\n", "    <li><a class=\"active\" href=\"heating72h.html\">&#128336; 72 Heizung</a></li>\n" ]
		
		self.heat24h = CHeating ( "heating24h.html", heatNav1, "images/heatHK1-24.svg", "24 Stunden Heizkreis HK", "images/heatHK2-24.svg", "24 Stunden Heizkreis FB", "images/heatHK3-24.svg", "24 Stunden Heizkreis WW"  )
		self.heat72h = CHeating ( "heating72h.html", heatNav2, "images/heatHK1-72.svg", "72 Stunden Heizkreis HK", "images/heatHK2-72.svg", "72 Stunden Heizkreis FB", "images/heatHK3-72.svg", "72 Stunden Heizkreis WW"  )

#-------------------------------------------------------------------------------------

	# Update 24/72 Stunden Klimatabelle mit aktuellen Module Daten
	# Die Module Daten werden aus dem angegebenen Container des DataHandlers gelesen und landen im Übergabebereich der jeweiligen Tabelle
	def updateClimePageModuleData( self, aIndexes, aCon, aTarget  ):
		ind = aIndexes
		aTarget.updateMTableData( self.mDH.getModuleNames( ind ), aCon.getCurrentDataVectorAt( ind ), self.mDH.getModuleLastUpdates( ind ), self.mDH.getModuleUpdates( ind ), self.mDH.getModuleStartTime( ind ), self.mDH.getModuleIpAddress( ind ) )
		
#-------------------------------------------------------------------------------------

	# Update 24/72 Stunden Klimatabelle mit aktuellen Temperatur Daten
	# Die Temperatur Daten werden aus dem angegebenen Container des DataHandlers gelesen.
	def updateClimePageTemperaturData( self, aIndexes, aCon, aTarget ):
		ind = aIndexes
		aTarget.updateTTableData( aCon.getNumberOfUnitsVectorAt( ind ), aCon.getCurrentDataVectorAt( ind ), aCon.getMaximumVectorAt( ind ), aCon.getMinimumVectorAt( ind ) )

#-------------------------------------------------------------------------------------

	# Update Klima 24 Stunden Seite mit aktuellen Luftfeuchtedaten
	# Die Luftfeuchte Daten werden aus dem angegebenen Container des DataHandlers gelesen.
	def updateClimePageHuminityData( self, aIndexes, aCon, aTarget ):
		ind = aIndexes
		aTarget.updateHTableData( aCon.getNumberOfUnitsVectorAt( ind ), aCon.getCurrentDataVectorAt( ind ), aCon.getMaximumVectorAt( ind ), aCon.getMinimumVectorAt( ind ) )

#-------------------------------------------------------------------------------------

	# Erstelle alle HTML Seiten neu und render die dazugehörenden Bilder
	def onUpdate( self ):
		self.omaMain.openTable();
		self.omaMain.updateTable( "oma", "192.168.1.4", "10.345.23.823", "Sun Oct 21 17:05:21", "100", "debian 13.5.6.7", "80%", "17", "18:34", "514 MB", "77%" )
		self.omaMain.closeTable()
		
		# Hole Backend Daten für die Klima Tabellen
		self.updateClimePageModuleData( self.mClimaIndexes, self.mDH.mA3C24, self.clim24h )
		self.updateClimePageModuleData( self.mClimaIndexes, self.mDH.mA3C72, self.clim72h )
		self.updateClimePageTemperaturData( self.mClimaIndexes, self.mDH.mA1C24, self.clim24h )
		self.updateClimePageTemperaturData( self.mClimaIndexes, self.mDH.mA1C72, self.clim72h )
		self.updateClimePageHuminityData( self.mClimaIndexes, self.mDH.mA2C24, self.clim24h )
		self.updateClimePageHuminityData( self.mClimaIndexes, self.mDH.mA2C72, self.clim72h )
		
		self.updateClimePageModuleData( self.mHeatIndexes, self.mDH.mA3C24, self.heat24h )
		self.updateClimePageModuleData( self.mHeatIndexes, self.mDH.mA3C72, self.heat72h )
		self.updateClimePageTemperaturData( self.mHeatIndexes, self.mDH.mA1C24, self.heat24h )
		self.updateClimePageTemperaturData( self.mHeatIndexes, self.mDH.mA1C72, self.heat72h )
		
		# Notwendige Bilder rendern
		self.renderImages( self.mClimaIndexes, self.mDH.mA1C24, "temp24h-", "&#176;", self.mClimImgInd )
		self.renderImages( self.mClimaIndexes, self.mDH.mA1C72, "temp72h-", "&#176;", self.mClimImgInd )
		self.renderImages( self.mClimaIndexes, self.mDH.mA2C24, "hum24h-", "%", self.mClimImgInd )
		self.renderImages( self.mClimaIndexes, self.mDH.mA2C72, "hum72h-", "%", self.mClimImgInd )
		self.renderImages( self.mHeatIndexes, self.mDH.mA1C24, "heat24h-", "&#176;", self.mHeatImgInd )
		self.renderImages( self.mHeatIndexes, self.mDH.mA1C72, "heat72h-", "&#176;", self.mHeatImgInd )
		
		self.renderCombinedImage( "tmp24.svg", "&#176;", self.mDH.mA1C24, self.mClimaIndexes, self.mClimaIndexes )
		self.renderCombinedImage( "hum24.svg",      "%", self.mDH.mA2C24, self.mClimaIndexes, self.mClimaIndexes )
		self.renderCombinedImage( "tmp72.svg", "&#176;", self.mDH.mA1C72, self.mClimaIndexes, self.mClimaIndexes )
		self.renderCombinedImage( "hum72.svg",      "%", self.mDH.mA2C72, self.mClimaIndexes,self.mClimaIndexes )
		
		colors = self.mColors
		self.mColors = ["red", "green", "blue", "pink", "white", "white", "white", "white", "white", "white", "green", "red", "green", "red", "green", "red", "white", "white", "white", "white"]
		self.renderCombinedImage( "heatHK1-24.svg", "&#176;", self.mDH.mA1C24, self.mHK1Indexes, self.mHeatIndexes )
		self.renderCombinedImage( "heatHK1-72.svg", "&#176;", self.mDH.mA1C72, self.mHK1Indexes, self.mHeatIndexes )
		self.renderCombinedImage( "heatHK2-24.svg", "&#176;", self.mDH.mA1C24, self.mHK2Indexes, self.mHeatIndexes )
		self.renderCombinedImage( "heatHK2-72.svg", "&#176;", self.mDH.mA1C72, self.mHK2Indexes, self.mHeatIndexes )
		self.renderCombinedImage( "heatHK3-24.svg", "&#176;", self.mDH.mA1C24, self.mHK3Indexes, self.mHeatIndexes )
		self.renderCombinedImage( "heatHK3-72.svg", "&#176;", self.mDH.mA1C72, self.mHK3Indexes, self.mHeatIndexes )
		self.mColors = colors

		# Alle HTML Seiten neu erstellen
		self.renderClimaPages()

#-------------------------------------------------------------------------------------

	def renderClimaPages( self ):
		self.clim24h.openTable();
		self.clim24h.updateTTableD( "images/temp24h" )
		self.clim24h.spacer()
		self.clim24h.updateHTableD( "images/hum24h" )
		self.clim24h.spacer()
		self.clim24h.updateMTable()
		self.clim24h.closeTable();

		self.clim72h.openTable();
		self.clim72h.updateTTableD( "images/temp72h" )
		self.clim72h.spacer()
		self.clim72h.updateHTableD( "images/hum72h" )
		self.clim72h.spacer()
		self.clim72h.updateMTable()
		self.clim72h.closeTable();
		
		self.heat24h.openTable();
		self.heat24h.updateTTable( "images/heat24h" )
		self.heat24h.spacer()
		self.heat24h.updateMTable()
		self.heat24h.closeTable();
		
		self.heat72h.openTable();
		self.heat72h.updateTTable( "images/heat72h" )
		self.heat72h.spacer()
		self.heat72h.updateMTable()
		self.heat72h.closeTable();

		#-------------------------------------------------------------------------------------

	def renderImages( self, aIndexes, aCon, aName, aSign, aImgIndex ):
		renderer = CSvgRender( 600, 300 )
		lfn = 0
		for index in aIndexes:
			name = aName + aImgIndex[ lfn ] + ".svg"
			renderer.renderImage( name, True, self.mColors[index], aCon.getUnits(), index, aCon.getMinimumAt( index ), aCon.getMaximumAt( index ), aSign,  aCon.getStartTimeAt( index ), aCon.getCurrentTimeAt( index ), aCon.getCurrentDataAt( index ), True )
			lfn += 1

#-------------------------------------------------------------------------------------

	def renderCombinedImage( self, aName, aDim, aContainer, aIndexes, aMinMaxIndexes ):
		colors = self.mColors
		renderer = CSvgRender( 600, 300 )
		renderer.printHeader( aName )
		min = aContainer.getMin4Ind( aMinMaxIndexes )
		max = aContainer.getMax4Ind( aMinMaxIndexes )

		for index in aIndexes:
			renderer.render( True, colors[index], aContainer.getUnits(), index, min, max )
		renderer.printFooter( min, max, aContainer.getStartTime(), aContainer.getCurrentTime(), 20, aDim, False )

#-------------------------------------------------------------------------------------