#!/usr/bin/python
# -*- coding: utf-8 -*-

# Version 1.0.0
import os, math, random, sys, time, math
from datetime import datetime

def getRandom( min, max ):
	diff = max - min
	x = random.random() * diff
	return min + x
	
class CMqttSimul( object ):
	def __init__( self, f ):
		self.f = f
		print( "Connected to MQTT Broker: " + "CMqttSimul" )

		

	def go( self ):
		ip10 = ["192.168.1.17", "192.168.1.22", "192.168.1.34", "192.168.1.78"]
		ip20 = ["192.168.1.56", "192.168.1.56", "192.168.1.56", "192.168.1.56"]
		counter = 0
		while True:
			counter = counter + 1
			val = []
			for x in range( 0, 4 ):
				val = getRandom( 10, 30 )
				tmp = "%.2f" % val
				val = getRandom( 50, 90 )
				hum = "%.2f" % val
				val = getRandom( 3, 5 )
				vcc = "%02.2f" % val
				module = "@101" + str( x ) + "07"
				self.f( "Sensor", "home/data", module + tmp + hum + vcc + ip10[x] )
				time.sleep ( 0.5 )
			for x in range( 0, 4 ):
				val = getRandom( 10, 30 )
				tmp = "%.2f" % val
				val = getRandom( 3, 5 )
				vcc = "%02.2f" % val
				module = "@102" + str( x ) + "05"
				if counter < 20 or counter > 40:
					self.f( "Sensor", "home/data", module + tmp + "99.99" + vcc + ip20[x] )
				time.sleep ( 0.5 )

