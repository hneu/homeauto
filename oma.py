import os, math, subprocess, codecs, time
from datetime import datetime

#-------------------------------------------------------------------------------------

class CPageOma( object ):

	def __init__( self, aTarget, aNavigation ):
		self.mTable = aTarget + ".txt"
		self.mTarget = aTarget
		self.mNavigation = aNavigation
		
#-------------------------------------------------------------------------------------

	def pageHeader( self ):
		self.d.write( "<!DOCTYPE html>\n" )
		self.d.write( "<html>\n" )
		self.d.write( "  <head>\n" )
		self.d.write( "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\n" )
		self.d.write( "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" )
		self.d.write( "    <meta http-equiv=\"Cache-Control\" content=\"no-cache, no-store, must-revalidate\"/>\n" )
		self.d.write( "    <link rel=\"stylesheet\" href=\"stylesheet.css\">\n" )
		self.d.write( "  </head>\n" )

#-------------------------------------------------------------------------------------

	def pageNavigation( self ):
		self.d.write( "  <ul class=\"sidenav\">\n" )
		
		for item in self.mNavigation:
			self.d.write( item )
	
		self.d.write( "  </ul>\n" )

#-------------------------------------------------------------------------------------
	
	def pageBody( self, open=True ):
		if open:
			self.d.write( "  <body>\n" )
		else:
			self.d.write( "  <\body>\n" )

#-------------------------------------------------------------------------------------
	def tableHeader( self, aCol1, aCol2 ):
		self.d.write( "<table>\n" )
		self.d.write( "  <tr>\n" )
		self.d.write( "    <th>" + aCol1 + "</th>\n" )
		self.d.write( "    <th>" + aCol2 + "</th>\n" )
		self.d.write( "  </tr>\n" )
		
#-------------------------------------------------------------------------------------
		
	def tableRow( self, aCol1, aCol2, end=False ):
		self.d.write( "  <tr>\n" )
		self.d.write( "    <td>" + aCol1 + "</td>\n" )
		self.d.write( "    <td>" + aCol2 + "</td>\n" )
		self.d.write( "  </tr>\n" )
		if end:
			self.d.write( "</table>\n" )

#-------------------------------------------------------------------------------------
			
	def spacer( self ):
		self.d.write( "<div class=\"spacer\"></div>\n")

#-------------------------------------------------------------------------------------

	def openTable( self ):
		self.d = codecs.open( self.mTable, "w", "utf-8" )
		self.pageHeader()
		self.pageBody()
		self.pageNavigation()
		
#-------------------------------------------------------------------------------------

	def updateTable( self, hostName, ipAdress, publicIpAdress, upTime, siteCalls, version, loadAverage, processes, serverTime, memoryUse, cpuUse ):
		
		self.d.write( "<div class=\"content\">\n" )
		self.d.write( "<h3>V I T A L S</h3>\n" )
		self.spacer()
		self.spacer()
  
		self.tableHeader( "Hostname", hostName );
		self.tableRow( "IP-adress", ipAdress );
		self.tableRow( "Public IP-adress", publicIpAdress );
		self.tableRow( "Uptime", upTime );
		self.tableRow( "Website calls", siteCalls );
		self.tableRow( "Version", version );
		self.tableRow( "Load Average", loadAverage );
		self.tableRow( "Processes", processes );		
		self.tableRow( "Server time", serverTime );
		self.tableRow( "Memory", memoryUse );
		self.tableRow( "CPU", cpuUse, True );

#-------------------------------------------------------------------------------------
	
	def closeTable( self ):
		self.footer()
		self.d.close()
		subprocess.call( "cp " + self.mTable + " " + self.mTarget, shell=True )				
#-------------------------------------------------------------------------------------	
		
	def footer( self ):
		now = datetime.now()
		t = now.strftime( "am %D um %H:%M:%S" )

		self.d.write( "    <div class=\"content\">\n" )
		self.d.write( "      <div class=\"spacer\"></div>\n" )
		self.d.write( "      <h3>Letzte Aktualisierung " + t + "</h3>\n" )
		self.d.write( "    </div>\n" )
		self.d.write( "  </body>\n" )
		self.d.write( "</html>\n" )		
	
