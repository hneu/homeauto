#!/usr/bin/python
# -*- coding: utf-8 -*-

# Version 1.2.0
import os, math, random, sys, time, math
from datetime import datetime

#-------------------------------------------------------------------------------------
MQTT = True
BROKER_ADDRESS = "192.168.1.6"
#-------------------------------------------------------------------------------------

arguments = len( sys.argv ) - 1
if arguments == 1 and sys.argv[arguments] == "sim":
	MQTT = False

if MQTT:
	import paho.mqtt.client as mqtt
	updateTime = 70
else:
	sys.path.append( os.getcwd() )
	updateTime = 5

from dataHandler import CDataHandler
from pageHandler import CPageHandler
from mqttSimul import CMqttSimul

#-------------------------------------------------------------------------------------

dataHandler = CDataHandler( updateTime  )
pageHandler = CPageHandler( dataHandler )

	
#-------------------------------------------------------------------------------------

def on_message( client, userdata, message ):
	
	if MQTT:
		msg = str( message.payload.decode( "utf-8" ) )
	else:
		msg = message
	
	if msg[0:2] == "@5":
		pageHandler.onUpdate()
	else:
		dataHandler.appendData( msg )
			
#-------------------------------------------------------------------------------------
 
def on_connect( client, userdata, flags, rc ):
	client.subscribe( '/home/data' )

#-------------------------------------------------------------------------------------

if MQTT:
	client = mqtt.Client()
	client.on_connect = on_connect
	client.on_message = on_message
	client.connect( BROKER_ADDRESS )
	print( "Connected to MQTT Broker: " + BROKER_ADDRESS )
	client.loop_forever()

else:
	mqttSimul = CMqttSimul( on_message )
	mqttSimul.go()
