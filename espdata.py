import os, math
from datetime import datetime

class CEspData( object ):

	def __init__( self, aName, aBufferSize ):
		self.mName = aName
		self.mBufferSize = aBufferSize
		self.mData = []
		self.mLastData = 0
		self.mTime = []
		self.mLastTime =""
		self.mTimeMin = ""
		self.mTimeMax = ""
		self.mTrace = False
		
	def getName( self ):
		return self.mName

	def getTimeStamp( self ):
		now = datetime.now()
		t = now.strftime( "%A %H:%M" )
		return t

	def getTimeMax( self ):
		return self.mTimeMax
		
	def getTimeMin( self) :
		return self.mTimeMin
				
	def getMinimum( self ):
		if len( self.mData  ) < 1:
			return 0.0
		self.min = 100000.0;
		x = 0
		for item in self.mData:
			if item < self.min:
				self.min = item
				self.mTimeMin = self.mTime[x]
			x += 1
		return self.min

	def getMaximum( self ):
		if len( self.mData  ) < 1:
			return 0.0
		self.max = -100000.0;
		x = 0
		for item in self.mData:
			if item > self.max:
				self.max = item
				self.mTimeMax = self.mTime[x]
			x += 1
		return self.max

	def appendData( self, aData ):
		day = datetime.today().strftime( '%A ' )
		
		if len( self.mData ) >= self.mBufferSize:
			del self.mData[0]
			del self.mTime[0]
		self.mData.append( aData )
		self.mTime.append( self.getTimeStamp() )

	def getNumberOfDataPoints( self ):
		return len( self.mData )
		
	def getData( self ):
		return self.mData

	def getTime( self ):
		return self.mTime		
		
	def getCurrentTime( self ):
		if len( self.mTime ) > 0:
			return self.mTime[ -1 ]
		return ""
		
	def getStartTime( self ):
		if len( self.mTime ) > 0:
			return self.mTime[ 0 ]
		return ""
		
	def getCurrentData( self ):
		if len( self.mData ) > 0:
			return self.mData[ -1 ]
		return 0

