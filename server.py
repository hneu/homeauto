import http.server
import socketserver
import os

class MyHttpRequestHandler( http.server.SimpleHTTPRequestHandler ):
	def do_GET( self ):
		print( "GET has been called..." )
		os.system( "python3 publish.py @5000" )
		return http.server.SimpleHTTPRequestHandler.do_GET( self )

handler_object = MyHttpRequestHandler

PORT = 8080
server = socketserver.TCPServer( ( "", PORT ), handler_object )

# Start the server
try:
	print( "server started at port", PORT, "..." )
	server.serve_forever()

except KeyboardInterrupt:
	pass
print( "Stopping server...\n" )
server.server_close()