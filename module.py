import os, math, time
from datetime import datetime
from espdata import CEspData


class CModule( object ):

	def __init__( self, aName, aColor, aIndex = "0"  ):
		self.mName = aName
		self.mVcc = "0"
		self.mLastUpdate = "-"
		self.mNumberOfUpdates = 0
		self.t24h = CEspData( "tmp24.svg", 1440 )
		self.t72h = CEspData( "tmp72.svg", 4320 )
		self.h24h = CEspData( "hum24.svg", 1440 )
		self.h72h = CEspData( "hum72.svg", 4320 )
		self.mColor = aColor
		self.mIndex = aIndex
		self.mStartTime = 0

	def getModuleName( self ):
		return self.mName

	def getStartTime( self ):
		return self.mStartTime
		
	def getTimeStamp( self ):
		now = datetime.now()
		t = now.strftime( "%A %H:%M" )
		return t
		
	def getDataT24Available( self ):
		if self.getNumberOfDataPointsT24() > 0:
			return True
		return False
		
	def getDataH24Available( self ):
		if self.getNumberOfDataPointsH24() > 0:
			return True
		return False
		
	def getDataT72Available( self ):
		if self.getNumberOfDataPointsT72() > 0:
			return True
		return False
		
	def getDataH72Available( self ):
		if self.getNumberOfDataPointsH72() > 0:
			return True
		return False
		
		
	def getMinimumT( self ):
		min24 = self.t24h.getMinimum();
		min72 = self.t72h.getMinimum();
		if min24 < min72:
			return min24
		return min72
		
	def getMaximumT( self ):
		max24 = self.t24h.getMaximum();
		max72 = self.t72h.getMaximum();
		if max24 > max72:
			return max24
		return max72
		
	def getMinimumH( self ):
		min24 = self.h24h.getMinimum();
		min72 = self.h72h.getMinimum();
		if min24 < min72:
			return min24
		return min72
		
	def getMaximumH( self ):
		max24 = self.h24h.getMaximum();
		max72 = self.h72h.getMaximum();
		if max24 > max72:
			return max24
		return max72
		
	def getEspDataT24( self ):
		return self.t24h
		
	def getEspDataT72( self ):
		return self.t72h
		
	def getEspDataH24( self ):
		return self.h24h
		
	def getEspDataH72( self ):
		return self.h72h
		
	def getMinimumT24( self ):
		return self.t24h.getMinimum()
		
	def getMinimumT72( self ):
		return self.t72h.getMinimum()
		
	def getMaximumT24( self ):
		return self.t24h.getMaximum()
		
	def getMaximumT72( self ):
		return self.t72h.getMaximum()
		
	def getMinimumH24( self ):
		return self.h24h.getMinimum()
		
	def getMinimumH72( self ):
		return self.h72h.getMinimum()
		
	def getMaximumH24( self ):
		return self.h24h.getMaximum()
		
	def getMaximumH72( self ):
		return self.h72h.getMaximum()
		
	def appendData( self, aData ):
		if self.mStartTime == 0:
			self.mStartTime = time.time();
		self.mLastUpdate = self.getTimeStamp()
		self.mNumberOfUpdates += 1
		if aData[5:8] == "nan":
			return
		if aData[2:4] == "02":
			sval = aData[5:10]
			fval = float( sval )
			self.t24h.appendData( fval )
			self.t72h.appendData( fval )
		elif aData[2:4] == "05":
			sval = aData[5:10]
			fval = float( sval )
			self.h24h.appendData( fval )
			self.h72h.appendData( fval )
		elif aData[2:4] == "06":
			val =  aData[5:10]
			self.setVcc( val )

	def getNumberOfDataPointsT24( self ):
		return self.t24h.getNumberOfDataPoints()
		
	def getNumberOfDataPointsT72( self ):
		return self.t72h.getNumberOfDataPoints()
		
	def getNumberOfDataPointsH24( self ):
		return self.h24h.getNumberOfDataPoints()
		
	def getNumberOfDataPointsH72( self ):
		return self.h72h.getNumberOfDataPoints()
		
	def getCurrentDataT( self ):
		return self.t24t.getCurrentData()
		
	def getCurrentDataH( self ):
		return self.t24h.getCurrentData()
  
	def setVcc( self, aVcc ):
		self.mVcc = aVcc

	def getVcc( self ):
		return self.mVcc;
		
	def getLastUpdateDate( self ):
		return self.mLastUpdate
	
	def getNumberOfUpdates( self ):
		return str( self.mNumberOfUpdates )
		
	def resetNumberOfUpdates( self ):
		self.mNumberOfUpdates = 0
		
	def getColor( self ):
		return self.mColor
	
	def getIndex( self ):
		return self.mIndex
	
	def getStartTimeT24( self ):
		return self.t24h.getStartTime()
		
	def getLastTimeT24( self ):
		return self.t24h.getCurrentTime()
	
	def getCurrentDataT24( self ):
		return self.t24h.getCurrentData()
		
	def getStartTimeT72( self ):
		return self.t72h.getStartTime()
		
	def getLastTimeT72( self ):
		return self.t72h.getCurrentTime()
	
	def getCurrentDataT72( self ):
		return self.t72h.getCurrentData()	
		
	def getStartTimeH24( self ):
		return self.h24h.getStartTime()
		
	def getLastTimeH24( self ):
		return self.h24h.getCurrentTime()
	
	def getCurrentDataH24( self ):
		return self.h24h.getCurrentData()
		
	def getStartTimeH72( self ):
		return self.h72h.getStartTime()
		
	def getLastTimeH72( self ):
		return self.h72h.getCurrentTime()
	
	def getCurrentDataH72( self ):
		return self.h72h.getCurrentData()				
